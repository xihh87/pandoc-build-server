REGISTRY=registry.gitlab.com/xihh87

help:Q: ## Show this help
	cmd/help mkfile | sort

push: ## push my local image to my docker registry
	docker push "${REGISTRY}/pandoc-build-server:$(git rev-parse --abbrev-ref HEAD)"

docker: ## build image to use pandoc and manubot from docker
	git stash -k -u
	docker build --no-cache -t "${REGISTRY}/pandoc-build-server:$(git rev-parse --short HEAD)" .
	docker tag "${REGISTRY}/pandoc-build-server:$(git rev-parse --short HEAD)" \
		"${REGISTRY}/pandoc-build-server:$(git rev-parse --abbrev-ref HEAD)"
	git stash pop || true

install: ## install my scripts to use execline as MKSHELL
	PREFIX=${PREFIX:-build}
	mkdir -p ${PREFIX}/bin
	install cmd/* ${PREFIX}/bin
