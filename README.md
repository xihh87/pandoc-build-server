![Build Status](https://gitlab.com/xihh87/pandoc-build-server/badges/main/pipeline.svg)

**pandoc-build-server** is a docker image for converting my markdown texts into beautiful PDF documents.

To build the image locally:

```
$ mk docker
```

The image is built by GitLab CI,
and can be accessed with:

```
$ docker login -u ${GITLAB_USER} registry.gitlab.com
$ docker pull registry.gitlab.com/xihh87/pandoc-build-server:latest
```

Use cases
---------

This is the working code for building some things.

```{file=.gitlab.yml}
build:
  image:
    name: registry.gitlab.com/xihh87/pandoc-build-server:template
    entrypoint: [""]
  script:
    - mkdir cache
    - cmd/targets | xargs env TEXMFCACHE=cache mk
    - rm -rf cache
  artifacts:
    untracked: true
    when: always
```

How to contribute
-----------------

You can [propose changes directly (preferred)](https://gitlab.com/xihh87/pandoc-build-server/-/merge_requests/new )
or [document open problems and proposals](https://gitlab.com/xihh87/pandoc-build-server/-/issues/new ).
