FROM pandoc/latex:2.19.2.0-alpine

RUN apk update \
&& apk add --update --no-cache \
	dmenu \
	font-adobe-source-code-pro \
	ttf-dejavu \
	font-noto \
	font-noto-cjk \
	py3-pip \
	s6 \
	ttf-font-awesome \
&& apk add --update --no-cache \
	--repository http://dl-3.alpinelinux.org/alpine/edge/testing/ \
	9base \
&& apk add --update --no-cache \
	--repository http://dl-3.alpinelinux.org/alpine/edge/community/ \
	font-roboto \
	font-roboto-mono \
&& rm -rf /var/cache/apk/* \
&& fc-cache -v

RUN pip install manubot==0.5.5 pandoc-include

RUN tlmgr update --self

RUN tlmgr install \
	adjustbox \
	background \
	beamertheme-metropolis \
	catchfile \
	collectbox \
	combofont \
	dejavu \
	enumitem \
	environ \
	everypage \
	fontawesome5 \
	fontawesome \
	footnotebackref \
	hardwrap \
	ifmtarg \
	koma-script \
	latexmk \
	linegoal \
	mdframed \
	mlmodern \
	needspace \
	pagecolor \
	pgfopts \
	pgfplots \
	roboto \
	selnolig \
	sourcecodepro \
	sourcesanspro \
	tcolorbox \
	titling \
	xifthen \
	zref \
&& rm -f /opt/texlive/texdir/texmf-var/web2c/tlmgr.log /opt/texlive/texdir/texmf-var/web2c/tlmgr.log

RUN cd /opt/texlive/texdir/texmf-dist/tex/latex/; \
	wget https://mirrors.ctan.org/macros/latex/contrib/acrotex.zip -O - | unzip - ; \
	mktexlsr "$(kpsewhich -var-value=TEXMFLOCAL)"

COPY \
	cmd/activate \
	cmd/debug \
	cmd/debug-env \
	cmd/help \
	cmd/input \
	cmd/make-config \
	cmd/mkc \
	cmd/mkvars \
	cmd/mkx \
	cmd/outdir \
	cmd/stdrun \
	cmd/targets \
	cmd/tmpfile \
	/usr/bin/

ADD https://raw.githubusercontent.com/xihh87/pandoc-latex-template/xihh/eisvogel.tex /usr/share/pandoc/data/templates/eisvogel.latex
ADD https://gitlab.com/xihh87/ucha-style/-/raw/master/pandoc/templates/ucha.latex /usr/share/pandoc/data/templates/ucha.latex

RUN chmod '0644' \
	/usr/share/pandoc/data/templates/eisvogel.latex \
	/usr/share/pandoc/data/templates/ucha.latex

ENV PATH=/opt/texlive/texdir/bin/x86_64-linuxmusl:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/lib/9base/bin
